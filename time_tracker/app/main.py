from dateutil import parser

from time_tracker.calendar_pkg.rate_safe_insert_event_module import \
    rate_safe_insert_event
from time_tracker.calendar_pkg.get_calendar_resource_module import \
    get_calendar_resource
from time_tracker.calendar_pkg.get_last_months_events_module import \
    get_events
from time_tracker.app.event_in_events_module import event_in_events
from time_tracker.app.get_event_for_commit_module import get_event_for_commit
from time_tracker.datetime_pkg.get_current_month_start_datetime import \
    get_current_month_start_datetime
from time_tracker.datetime_pkg.get_current_year_start_datetime import \
    get_current_year_start_datetime
from time_tracker.email_pkg.email_user_about_tracking_their_time import \
    email_user_about_tracking_their_time
from time_tracker.gitlab_pkg.generate_commits_module import \
    generate_groups_projects_commits


def main(
    *,
    since,
    until,
):
    events = get_events(
        since=since,
        until=until,
    )

    resource = get_calendar_resource()

    groups_projects_commits = generate_groups_projects_commits(
        since=since,
        until=until,
    )
    for group, project, commit in groups_projects_commits:
        commit.created_at_datetime = parser.parse(
            commit.created_at
        )

        event = get_event_for_commit(
            group=group,
            project=project,
            commit=commit,
        )

        if event_in_events(event=event, events=events):
            print(
                'DEBUG: Commit was known in calendar:',
                commit.created_at,
                commit.title,
            )
            continue

        print(
            'INFO: Commit was new in calendar:',
            commit.created_at,
            commit.title,
        )

        rate_safe_insert_event(event=event, resource=resource)

    print(
        'INFO: '
        'Emailing user about being done with transferring commits to events.'
    )
    email_user_about_tracking_their_time()


if __name__ == '__main__':
    main(
        since=get_current_year_start_datetime(),
        until=get_current_month_start_datetime(),
    )
