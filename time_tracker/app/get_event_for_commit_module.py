from datetime import timedelta

from time_tracker.calendar_pkg.get_calendar_timezone_module import \
    get_calendar_timezone


def get_event_for_commit(
    *,
    group,
    project,
    commit,
):
    commit_link = f'https://gitlab.com/' \
          f'{group.name}/' \
          f'{project.name}/' \
          f'-/commit/{commit.id}'

    event: dict = {
        'summary': commit.title,
        # 'location': '',
        'description': commit_link,
        'start': {
            'dateTime': (
                commit.created_at_datetime.astimezone(
                    get_calendar_timezone()
                ) - timedelta(hours=2)
            ).isoformat(),
            'timeZone': get_calendar_timezone().zone,
        },
        'end': {
            'dateTime':
                commit.created_at_datetime.astimezone(
                    get_calendar_timezone()
                ).isoformat(),
            'timeZone': get_calendar_timezone().zone,
        },
        # 'recurrence': [
        #     'RRULE:FREQ=DAILY;COUNT=2'
        # ],
        # 'attendees': [
        #     {'email': 'lpage@example.com'},
        #     {'email': 'sbrin@example.com'},
        # ],
        # 'reminders': {
        #     'useDefault': False,
        #     'overrides': [
        #         {'method': 'email', 'minutes': 24 * 60},
        #         {'method': 'popup', 'minutes': 10},
        #     ],
        # },
    }

    return event
