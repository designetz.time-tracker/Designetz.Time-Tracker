import dateutil


def event_in_events(event, events):
    contained: bool = any(
        dateutil.parser.parse(event_instance['end']['dateTime']).isoformat() ==
        dateutil.parser.parse(event['end']['dateTime']).isoformat()
        for event_instance in events
    )

    return contained
