import json
import os

from time_tracker.gitlab_pkg.paths import GITLAB_CREDENTIALS_FILE_PATH

try:
    loaded_dict = json.loads(os.environ['GITLAB_CREDENTIALS_FILE_CONTENT'])
except KeyError:
    loaded_dict = json.load(open(GITLAB_CREDENTIALS_FILE_PATH, 'r'))

PRIVATE_TOKENS = \
    loaded_dict['private_tokens']
GITLAB_URLS = \
    loaded_dict['gitlab_urls']
PROJECT_GROUP_PREFIXES = \
    loaded_dict['project_group_prefixes']
COMMITTER_NAMES = \
    loaded_dict['committer_names']
COMMITTER_EMAILS = \
    loaded_dict['committer_emails']
