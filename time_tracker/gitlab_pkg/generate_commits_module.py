from time_tracker.gitlab_pkg.generate_commits_for_gitlab_module import \
    generate_groups_projects_commits_for_gitlab
from time_tracker.gitlab_pkg.get_gitlab_module import get_gitlabs


def generate_groups_projects_commits(
    *,
    since,
    until,
):
    gitlabs = get_gitlabs()
    for gitlab in gitlabs:
        yield from generate_groups_projects_commits_for_gitlab(
            gitlab=gitlab,
            since=since,
            until=until,
        )
