from time_tracker.gitlab_pkg.configuration import PROJECT_GROUP_PREFIXES


def group_is_relevant(group):
    relevance: bool = any(
        group.name.startswith(prefix) or group.name == prefix
        for prefix in PROJECT_GROUP_PREFIXES
    )

    return relevance
