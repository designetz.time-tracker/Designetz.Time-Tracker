from time_tracker.gitlab_pkg.configuration import COMMITTER_NAMES, \
    COMMITTER_EMAILS
from time_tracker.gitlab_pkg.gitlab_configuration_error import \
    GitlabConfigurationError
from time_tracker.gitlab_pkg.group_is_relevant_module import group_is_relevant


def generate_groups_projects_commits_for_gitlab(
    *,
    gitlab,
    since,
    until,
):
    irrelevant_group_names = set()
    relevant_group_names = set()
    irrelevant_committer_names = set()
    relevant_committer_names = set()
    irrelevant_committer_emails = set()
    relevant_committer_emails = set()
    relevant_projects = set()
    irrelevant_projects = set()

    groups = gitlab.groups.list(
        include_subgroups=True,
        all=True,
    )
    for group in groups:
        if not group_is_relevant(group=group):
            print(
                f'DEBUG: Handling irrelevant group with name "{group.name}".'
            )
            irrelevant_group_names.add(group.name)

            group_projects = group.projects.list(
                all=True,
            )
            print('Group Projects:', group_projects)
            for group_project in group_projects:
                project = gitlab.projects.get(
                    id=group_project.id,
                    lazy=False,
                )

                print(
                    f'INFO: Handling irrelevant project '
                    f'"{group.name}.{project.name}".'
                )

                irrelevant_projects.add(project)

            continue

        print(
            f'INFO: Handling relevant group with name "{group.name}".'
        )
        relevant_group_names.add(group.name)

        group_projects = group.projects.list(
            all=True
        )
        for group_project in group_projects:
            project = gitlab.projects.get(
                id=group_project.id,
                lazy=False,
            )
            relevant_projects.add(project)
            print(
                f'INFO: Handling relevant project "{project.name}".'
            )

            commits = project.commits.list(
                since=since.isoformat(),
                until=until.isoformat(),
                all=True,
            )

            for commit in commits:
                if commit.committer_name not in COMMITTER_NAMES:
                    if commit.committer_email in COMMITTER_EMAILS:
                        raise GitlabConfigurationError(
                            'Committer email is known, but name is not. '
                            'Please either remove the committer email '
                            f'({commit.committer_email}) or '
                            'add the committer name '
                            f'({commit.committer_name})'
                        )
                    print(
                        'DEBUG: '
                        'Skipping commit with irrelevant committer name '
                        f'"{commit.committer_name}". '
                        'You can configure this in the gitlab credentials.'
                    )
                    irrelevant_committer_names.add(commit.committer_name)
                    irrelevant_committer_emails.add(commit.committer_email)
                    continue
                if commit.committer_email not in COMMITTER_EMAILS:
                    if commit.committer_name in COMMITTER_NAMES:
                        raise GitlabConfigurationError(
                            'Committer name is known, but email is not. '
                            'Please either remove the committer name '
                            f'({commit.committer_name}) or '
                            'add the committer name '
                            f'({commit.committer_email})'
                        )
                    print(
                        'DEBUG: '
                        'Skipping commit with irrelevant committer email '
                        f'"{commit.committer_email}".'
                        'You can configure this in the gitlab credentials.'
                    )
                    irrelevant_committer_names.add(commit.committer_name)
                    irrelevant_committer_emails.add(commit.committer_email)
                    continue

                relevant_committer_names.add(commit.committer_name)
                relevant_committer_emails.add(commit.committer_email)

                yield group, project, commit

    print(
        'irrelevant group names:\n\t' +
        '\n\t'.join(irrelevant_group_names), '\n'
        'relevant group names:\n\t' +
        '\n\t'.join(relevant_group_names), '\n'
        'irrelevant projects:\n\t' +
        '\n\t'.join((project.name for project in irrelevant_projects)), '\n'
        'relevant projects:\n\t' +
        '\n\t'.join(project.name for project in relevant_projects), '\n'
        'irrelevant committer names:\n\t' +
        '\n\t'.join(irrelevant_committer_names), '\n'
        'relevant committer names:\n\t' +
        '\n\t'.join(relevant_committer_names), '\n'
        'irrelevant committer emails:\n\t' +
        '\n\t'.join(irrelevant_committer_emails), '\n'
        'relevant committer emails:\n\t' +
        '\n\t'.join(relevant_committer_emails), '\n'
    )
