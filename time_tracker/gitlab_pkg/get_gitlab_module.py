from itertools import product
from typing import List

from gitlab import Gitlab

from time_tracker.gitlab_pkg.configuration import GITLAB_URLS, PRIVATE_TOKENS


def get_gitlabs():
    gitlabs: List[Gitlab] = []
    for gitlab_url, private_token in product(GITLAB_URLS, PRIVATE_TOKENS):
        gitlab: Gitlab = Gitlab(
            url=gitlab_url,
            private_token=private_token,
        )
        gitlabs.append(gitlab)

    return gitlabs
