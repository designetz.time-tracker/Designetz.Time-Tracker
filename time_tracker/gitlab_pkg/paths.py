from time_tracker.util.paths import ROOT_PATH

GITLAB_CREDENTIALS_FILE_PATH = \
    ROOT_PATH / 'credentials' / 'gitlab' / 'credentials.json'
