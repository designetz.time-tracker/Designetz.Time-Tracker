from time_tracker.datetime_pkg.get_current_day_start_datetime import \
    get_current_day_start_datetime
from time_tracker.datetime_pkg.get_previous_day_start_datetime import \
    get_previous_day_start_datetime
from time_tracker.gitlab_pkg.generate_commits_module import \
    generate_groups_projects_commits


def main(
    *,
    since,
    until,
):
    groups_projects_commits = generate_groups_projects_commits(
        since=since,
        until=until,
    )

    for group, project, commit in groups_projects_commits:
        print('    ' + group.name, project.id, commit.created_at, commit.title)


if __name__ == '__main__':
    print('Yesterdays\'s Commits: ')
    main(
        since=get_previous_day_start_datetime(),
        until=get_current_day_start_datetime(),
    )
