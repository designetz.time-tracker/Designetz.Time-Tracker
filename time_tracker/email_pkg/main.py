from time_tracker.email_pkg.email_user_about_tracking_their_time import \
    email_user_about_tracking_their_time


def main():
    email_user_about_tracking_their_time()


if __name__ == '__main__':
    main()
