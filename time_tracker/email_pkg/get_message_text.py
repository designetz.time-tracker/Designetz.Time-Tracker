def get_message_text() -> str:
    text: str = \
        'Hallo,\n' \
        '\n' \
        'deine Commits des zurückliegenden Monats wurden erfolgreich als ' \
        'Termine in deinen Kalender eingetragen. Übertrage sie doch jetzt in ' \
        'deine Zeiterfassung unter: \n' \
        '\n' \
        'https://srvomi02.offis.uni-oldenburg.de/BusinessWorld/\n' \
        '\n' \
        'GitLab Time Tracker Bot'

    return text
