from googleapiclient.discovery import build

from time_tracker.email_pkg.get_credentials import get_credentials


def get_service():
    credentials = get_credentials()

    service: object = build('gmail', 'v1', credentials=credentials)

    return service
