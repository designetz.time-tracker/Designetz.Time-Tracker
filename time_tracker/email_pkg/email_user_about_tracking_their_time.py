from time_tracker.email_pkg.configuration import EMAIL_SENDER_ADDRESS, \
    EMAIL_RECEIVER_ADDRESS, EMAIL_SUBJECT
from time_tracker.email_pkg.create_message import create_message
from time_tracker.email_pkg.get_message_text import get_message_text
from time_tracker.email_pkg.get_service import get_service
from time_tracker.email_pkg.send_message import send_message


def email_user_about_tracking_their_time():
    message_text = get_message_text()
    message = create_message(
        sender=EMAIL_SENDER_ADDRESS,
        to=EMAIL_RECEIVER_ADDRESS,
        subject=EMAIL_SUBJECT,
        message_text=message_text,
    )
    service = get_service()

    send_message(
        message=message,
        user_id='me',
        service=service,
    )
