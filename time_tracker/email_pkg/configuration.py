import os

EMAIL_SENDER_ADDRESS = \
    os.getenv('EMAIL_SENDER_ADDRESS')
EMAIL_RECEIVER_ADDRESS = \
    os.getenv('EMAIL_RECEIVER_ADDRESS')
EMAIL_SERVER_PASSWORD = \
    os.getenv('EMAIL_SERVER_PASSWORD')
EMAIL_SUBJECT = \
    os.getenv('EMAIL_SUBJECT', 'Zeiterfassung machen')

# If modifying these scopes, delete the file token.pickle.
SCOPES = [
    'https://www.googleapis.com/auth/gmail.send',
]
