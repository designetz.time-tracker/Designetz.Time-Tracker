from apiclient import errors


def send_message(service, user_id, message):
    try:
        service.users().messages().send(userId=user_id, body=message).execute()
    except errors.HttpError as error:
        print('An error occurred: %s' % error)
