import datetime
import json
import os

from google.oauth2.credentials import Credentials
from google_auth_httplib2 import Request
from google_auth_oauthlib.flow import InstalledAppFlow

from time_tracker.email_pkg.configuration import SCOPES
from time_tracker.email_pkg.paths import EMAIL_CREDENTIALS_FILE_PATH, \
    EMAIL_SECRETS_FILE_PATH


def get_credentials():
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    try:
        print('DEBUG: Trying to load credentials from environment ...')
        credentials_dict = \
            json.loads(os.environ['EMAIL_SECRETS_FILE_CONTENTS'])
        print('INFO: Loaded credentials from environment.')
    except KeyError:
        if os.path.exists(EMAIL_SECRETS_FILE_PATH):
            with open(EMAIL_SECRETS_FILE_PATH, 'rb') as token:
                credentials_dict = json.load(token)
        credentials_dict['expiry'] = \
            datetime.datetime.fromisoformat(credentials_dict['expiry'])

    credentials = Credentials(
        token=credentials_dict['token'],
        refresh_token=credentials_dict['_refresh_token'],
        token_uri=credentials_dict['_token_uri'],
        client_id=credentials_dict['_client_id'],
        client_secret=credentials_dict['_client_secret']
    )

    # If there are no (valid) credentials available, let the user log in.
    if not credentials or not credentials.valid:
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                EMAIL_CREDENTIALS_FILE_PATH, SCOPES)
            credentials = flow.run_local_server(port=0)

        credentials_dict = credentials.__dict__
        credentials_dict['expiry'] = \
            credentials_dict['expiry'].isoformat()

        # Save the credentials for the next run
        with open(EMAIL_SECRETS_FILE_PATH, 'w') as token:
            json.dump(
                credentials_dict,
                token,
                indent=2,
            )

    return credentials
