import base64
from email.mime.text import MIMEText


def create_message(sender, to, subject, message_text):
    message_mime = MIMEText(message_text)
    message_mime['to'] = to
    message_mime['from'] = sender
    message_mime['subject'] = subject

    message_dict: dict = {
        'raw':
        base64.urlsafe_b64encode(message_mime.as_string().encode()).decode(),
    }
    return message_dict
