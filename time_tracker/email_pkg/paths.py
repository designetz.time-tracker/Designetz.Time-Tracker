from time_tracker.util.paths import ROOT_PATH

EMAIL_CREDENTIALS_FILE_PATH = \
    ROOT_PATH / 'credentials' / 'email' / 'email_credentials.json'
EMAIL_SECRETS_FILE_PATH = \
    ROOT_PATH / 'credentials' / 'email' / 'email_secrets.json'
