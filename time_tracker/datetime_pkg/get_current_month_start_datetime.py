from datetime import datetime
from functools import lru_cache


@lru_cache(1)
def get_current_month_start_datetime():
    now = datetime.utcnow()
    start_of_current_month_datetime: datetime = \
        datetime(year=now.year, month=now.month, day=1)

    return start_of_current_month_datetime
