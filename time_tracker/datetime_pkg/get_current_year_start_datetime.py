from datetime import datetime
from functools import lru_cache


@lru_cache(1)
def get_current_year_start_datetime():
    now = datetime.utcnow()
    start_of_current_month_datetime: datetime = \
        datetime(year=now.year, month=1, day=1)

    return start_of_current_month_datetime
