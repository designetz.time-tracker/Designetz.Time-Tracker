from datetime import datetime
from functools import lru_cache


@lru_cache(1)
def get_previous_day_start_datetime():
    now = datetime.utcnow()
    start_of_previous_month_datetime: datetime = \
        datetime(year=now.year, month=now.month, day=now.day - 1)

    return start_of_previous_month_datetime
