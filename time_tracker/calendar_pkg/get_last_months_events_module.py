from typing import List

from time_tracker.calendar_pkg.configuration import CALENDAR_ID
from time_tracker.calendar_pkg.get_calendar_resource_module import \
    get_calendar_resource


def get_events(
    *,
    since,
    until,
):
    resource = get_calendar_resource()

    events: List = []
    next_page_token = None
    max_results = 2500
    while True:
        print(f'INFO: Getting up to {max_results} events.')
        events_result = resource.events().list(
            calendarId=CALENDAR_ID,
            timeMin=since.isoformat() + 'Z',
            timeMax=until.isoformat() + 'Z',
            maxResults=max_results,
            singleEvents=True,
            orderBy='startTime',
            pageToken=next_page_token,
        ).execute()
        print('INFO: Got events.')

        events += events_result.get('items', [])

        try:
            next_page_token = events_result['nextPageToken']

            # There is a next page.
            # This means that there are more events to be processed.
            # So query the next results page.

            continue
        except KeyError:
            # There is no next page.
            # This means we got all events on the current page.
            # So stop querying a next results page.
            break

    print(f'Got {len(events)} in total.')

    return events
