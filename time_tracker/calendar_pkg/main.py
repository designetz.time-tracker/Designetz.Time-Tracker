from __future__ import print_function

from time_tracker.calendar_pkg.get_last_months_events_module import get_events
from time_tracker.datetime_pkg.get_current_month_start_datetime import \
    get_current_month_start_datetime
from time_tracker.datetime_pkg.get_previous_month_start_datetime import \
    get_previous_month_start_datetime


def main(
    *,
    since,
    until,
):
    events = get_events(
        since=since,
        until=until,
    )

    if len(events) == 0:
        print(f'INFO: Found {len(events)} events.')
        return

    print(f'INFO: Found these {len(events)} events:')
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        print('     ', start, event['summary'])


if __name__ == '__main__':
    main(
        since=get_previous_month_start_datetime(),
        until=get_current_month_start_datetime(),
    )
