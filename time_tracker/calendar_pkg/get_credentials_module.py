import json
import os

from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow

from time_tracker.calendar_pkg.configuration import SCOPES
from time_tracker.calendar_pkg.paths import CALENDAR_CREDENTIALS_FILE_PATH, \
    CLIENT_SECRETS_FILE_PATH


def get_credentials():
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.

    try:
        print('DEBUG: Trying to load credentials from environment ...')
        credentials_dict = json.loads(os.environ['CALENDAR_TOKEN_FILE_CONTENT'])
        print('INFO: Loaded credentials from environment.')
    except KeyError:
        try:
            print('DEBUG: Trying to load credentials from file ...')
            credentials_dict = \
                json.load(open(CALENDAR_CREDENTIALS_FILE_PATH, 'rb'))
            print('INFO: Loaded credentials from file.')
        except FileNotFoundError:
            print('DEBUG: Could not load credentials from file. '
                  'Falling back to provisioning new ones.')
            try:
                print('DEBUG: Trying to initiate an authorization flow ...')
                flow = InstalledAppFlow.from_client_secrets_file(
                    client_secrets_file=CLIENT_SECRETS_FILE_PATH,
                    scopes=SCOPES,
                )
                credentials = flow.run_local_server(port=0)
                print('INFO: Authorization flow finished.')

                print('DEBUG: Saving the provisioned credentials to a file ...')
                credentials_dict = {
                    'token': credentials.token,
                    'refresh_token': credentials.refresh_token,
                    'token_uri': credentials.token_uri,
                    'client_id': credentials.client_id,
                    'client_secret': credentials.client_secret,
                }
                json.dump(
                    credentials_dict,
                    open(CALENDAR_CREDENTIALS_FILE_PATH, 'w'),
                    indent=2,
                )
                print('INFO: Saved the provisioned credentials to a file.')
            except FileNotFoundError:
                raise FileNotFoundError(
                    'Could not find credential file. '
                    'Download credentials from Google: '
                    'https://console.developers.google.com/apis'
                    'Then store them under: '
                    f'{CLIENT_SECRETS_FILE_PATH}'
                )

    credentials = Credentials(
        token=credentials_dict['token'],
        refresh_token=credentials_dict['refresh_token'],
        token_uri=credentials_dict['token_uri'],
        client_id=credentials_dict['client_id'],
        client_secret=credentials_dict['client_secret']
    )

    if credentials.expired and credentials.refresh_token:
        print('WARN: Credentials are expired.')
        print('DEBUG: Trying to refresh the credentials ...')
        credentials.refresh(Request())
        print('INFO: Refreshed the credentials was successful.')

    return credentials
