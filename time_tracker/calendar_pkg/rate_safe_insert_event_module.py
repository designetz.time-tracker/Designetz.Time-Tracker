import time

import googleapiclient

from time_tracker.calendar_pkg.configuration import CALENDAR_ID
from time_tracker.calendar_pkg.retry_limit_exceeded_error import \
    RetryLimitExceededError


def rate_safe_insert_event(event, resource):
    for retry_count in range(9):
        try:
            resource.events().insert(
                calendarId=CALENDAR_ID,
                body=event,
            ).execute()
            return
        except googleapiclient.errors.HttpError:
            # We probably ran into the rate limit.
            print(
                'INFO:'
                f'Retrying to insert event into calender.'
                f'(Retry #{retry_count}).'
            )
            time.sleep(0.1)
            continue

    raise RetryLimitExceededError(
        'Failed to insert event into calendar:',
        event
    )
