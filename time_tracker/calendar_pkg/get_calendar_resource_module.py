from functools import lru_cache

from googleapiclient.discovery import Resource, build

from time_tracker.calendar_pkg.get_credentials_module import get_credentials


@lru_cache(1)
def get_calendar_resource():
    credentials = get_credentials()

    resource: Resource = build(
        serviceName='calendar',
        version='v3',
        credentials=credentials,
    )

    return resource
