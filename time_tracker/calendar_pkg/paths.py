from time_tracker.util.paths import ROOT_PATH

CALENDAR_CREDENTIALS_FILE_PATH = \
    ROOT_PATH / 'credentials' / 'calendar' / 'calendar_credentials.json'
CLIENT_SECRETS_FILE_PATH = \
    ROOT_PATH / 'credentials' / 'calendar' / 'calendar_secrets.json'
CALENDAR_ID_FILE_PATH = \
    ROOT_PATH / 'credentials' / 'calendar' / 'calendar_id.json'
