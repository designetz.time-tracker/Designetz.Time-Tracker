import json
import os

from time_tracker.calendar_pkg.paths import CALENDAR_ID_FILE_PATH

SCOPES = [
    'https://www.googleapis.com/auth/calendar.events',
    'https://www.googleapis.com/auth/calendar.settings.readonly',
]

try:
    CALENDAR_ID: str = \
        json.loads(os.environ['CALENDAR_ID_FILE_CONTENTS'])['calendar_id']
except KeyError:
    CALENDAR_ID: str =\
        json.load(open(CALENDAR_ID_FILE_PATH, 'r'))['calendar_id']
