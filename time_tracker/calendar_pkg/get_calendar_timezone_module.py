from functools import lru_cache

from pytz import timezone

from time_tracker.calendar_pkg.get_calendar_resource_module import \
    get_calendar_resource


@lru_cache(1)
def get_calendar_timezone():
    calendar_timezone: timezone = \
        timezone(
            get_calendar_resource().settings().get(
                setting='timezone'
            ).execute()
            ['value']
        )

    return calendar_timezone
