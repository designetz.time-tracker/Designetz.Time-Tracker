from time_tracker.calendar_pkg.main import main
from time_tracker.datetime_pkg.get_current_day_start_datetime import \
    get_current_day_start_datetime
from time_tracker.datetime_pkg.get_previous_day_start_datetime import \
    get_previous_day_start_datetime


def test_main():
    main(
        since=get_previous_day_start_datetime(),
        until=get_current_day_start_datetime(),
    )
