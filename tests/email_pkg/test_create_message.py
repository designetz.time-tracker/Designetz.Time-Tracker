from time_tracker.email_pkg.configuration import EMAIL_SENDER_ADDRESS, \
    EMAIL_RECEIVER_ADDRESS, EMAIL_SUBJECT
from time_tracker.email_pkg.create_message import create_message
from time_tracker.email_pkg.get_message_text import get_message_text


def test_create_message():
    # Mock
    message_text = get_message_text()

    # Test
    message = create_message(
        sender=EMAIL_SENDER_ADDRESS,
        to=EMAIL_RECEIVER_ADDRESS,
        subject=EMAIL_SUBJECT,
        message_text=message_text,
    )

    # Assert
    assert message is not None
    assert isinstance(message, dict)
    assert list(message.keys())[0] == 'raw'
