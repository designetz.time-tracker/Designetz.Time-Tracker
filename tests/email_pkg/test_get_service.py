from googleapiclient.discovery import Resource

from time_tracker.email_pkg.get_service import get_service


def test_get_service():
    service = get_service()

    # Assert
    assert service is not None
    assert isinstance(service, Resource)
