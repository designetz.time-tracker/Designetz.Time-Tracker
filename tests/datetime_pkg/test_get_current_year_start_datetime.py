from datetime import datetime

from time_tracker.datetime_pkg.get_current_year_start_datetime import \
    get_current_year_start_datetime


def test_get_current_year_start_datetime():
    # Test
    current_year_start_datetime = get_current_year_start_datetime()

    # Assert
    assert isinstance(current_year_start_datetime, datetime)
