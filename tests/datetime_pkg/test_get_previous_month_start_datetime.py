from datetime import datetime

from time_tracker.datetime_pkg.get_previous_month_start_datetime import \
    get_previous_month_start_datetime


def test_get_previous_month_start_datetime():
    # Test
    previous_month_start_datetime = get_previous_month_start_datetime()

    # Assert
    assert isinstance(previous_month_start_datetime, datetime)
