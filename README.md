# TimeTracker

## Set up a virtual environment

    python3.8 -m venv venv

## Install virtual environment requirements

    venv/bin/python -m pip install -r requirements.d/tox.txt

## Setup runtime environments

    venv/bin/python -m tox --notest

## Install latest virtual environment requirements

    venv/bin/python -m pip install --upgrade tox setuptools pip 

## Install latest linting requirements

    .tox/flake8/bin/python -m pip install flake8

## Install latest test requirements

    .tox/py38/bin/python -m pip install pytest

## Run the test suite

    venv/bin/python -m tox

## Freeze test requirements

    .tox/py38/bin/python -m pip freeze --all > requirements.d/pytest.txt

## Freeze lint requirements

    .tox/flake8/bin/python -m pip freeze --all > requirements.d/flake8.txt 

## Freeze virtual environment requirements

    venv/bin/python -m pip freeze --all > requirements.d/tox.txt
